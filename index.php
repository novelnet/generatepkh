<?php
declare(strict_types=1);

use BitWasp\Bitcoin\Key\Factory\HierarchicalKeyFactory;
use BitWasp\Bitcoin\Address\AddressCreator;
use BitWasp\Bitcoin\Network\NetworkFactory;
use BitWasp\Bitcoin\Address\PayToPubKeyHashAddress;
use BitWasp\Bitcoin\Address\SegwitAddress;
use BitWasp\Bitcoin\Script\WitnessProgram;
use BitWasp\Bitcoin\Address\ScriptHashAddress;
use BitWasp\Bitcoin\Script\P2shScript;
use BitWasp\Bitcoin\Script\WitnessScript;
use BitWasp\Bitcoin\Bitcoin;
use BitWasp\Bitcoin\Key\Deterministic\HdPrefix\GlobalPrefixConfig;
use BitWasp\Bitcoin\Key\Deterministic\HdPrefix\NetworkConfig;
use BitWasp\Bitcoin\Network\Slip132\BitcoinRegistry;
use BitWasp\Bitcoin\Key\Deterministic\Slip132\Slip132;
use BitWasp\Bitcoin\Key\KeyToScript\KeyToScriptHelper;
use BitWasp\Bitcoin\Serializer\Key\HierarchicalKey\Base58ExtendedKeySerializer;
use BitWasp\Bitcoin\Serializer\Key\HierarchicalKey\ExtendedKeySerializer;

require_once __DIR__ . '/vendor/autoload.php';

$isYpub = true;
if($isYpub) {
    // The ONLY way to parse masterkeys other than xpub (also altcoins) is creating a Base58ExtendedKeySerializer with
    // a global config
    $adapter = Bitcoin::getEcAdapter();
    $btc = NetworkFactory::bitcoin();
    $slip132 = new Slip132(new KeyToScriptHelper($adapter));
    $bitcoinPrefixes = new BitcoinRegistry();
    $config = new GlobalPrefixConfig([
        new NetworkConfig($btc, [$slip132->p2shP2wpkh($bitcoinPrefixes)]) // p2shP2wpkh (p2wpkh in p2sh) -> ypup
    ]);
    $serializer = new Base58ExtendedKeySerializer(
        new ExtendedKeySerializer($adapter, $config)
    );

    // master public key from m/49'/0'/0'
    $account0masterKey = $serializer->parse($btc, "ypub...");
}
else{
    // xpub
    $hdFactory = new HierarchicalKeyFactory();
    $account0masterKey = $hdFactory->fromExtended("xpub...");
}

// address 2 public key from "m/49'/0'/0'/0/1"
$addrKey = $account0masterKey->derivePath("0/1");

$address = $addrKey->getAddress(new AddressCreator());
echo "<br>p2shP2wpkh addr[1]: {$address->getAddress()}";

$addrKeyHash = $addrKey->getPublicKey()->getPubKeyHash();
$p2pkh = new PayToPubKeyHashAddress($addrKeyHash);
echo "<br>p2pkh addr[1]: {$p2pkh->getAddress()}\n";

$p2wpkhWP = WitnessProgram::v0($addrKeyHash);
$p2wpkh = new SegwitAddress($p2wpkhWP);
echo "<br>p2wpkh addr[1]: {$p2wpkh->getAddress()}\n";

$redeemScript = $p2pkh->getScriptPubKey();
$p2wshWP = WitnessProgram::v0($redeemScript->getWitnessScriptHash());
$p2shP2wsh = new ScriptHashAddress($p2wshWP->getScript()->getScriptHash());
echo "<br>p2sh addr[1]: {$p2shP2wsh->getAddress()}\n";

$p2wshScript = new WitnessScript($p2pkh->getScriptPubKey());
$p2wshAddr = $p2wshScript->getAddress();
echo "<br>p2wsh addr[1]: {$p2wshAddr->getAddress()}\n";

$p2shP2wshScript = new P2shScript(new WitnessScript($p2pkh->getScriptPubKey()));
$p2shP2wshAddr = $p2shP2wshScript->getAddress();
echo "<br> p2sh|p2wsh addr[1]: {$p2shP2wshAddr->getAddress()}\n";
