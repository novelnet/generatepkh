# generatePKH


##Prerequisite
* PHP 7.x
* Composer
* PHP GMP Extension

⋅⋅⋅`sudo apt-get install php-gmp`

##Installation
`composer require bitwasp/bitcoin`

##Usage Example
p2shP2wpkh, supports segwit/bech32 & non-segwit wallets (backwards compatibility)
```php
<?php
declare(strict_types=1);

use BitWasp\Bitcoin\Network\NetworkFactory;
use BitWasp\Bitcoin\Bitcoin;
use BitWasp\Bitcoin\Key\Deterministic\HdPrefix\GlobalPrefixConfig;
use BitWasp\Bitcoin\Key\Deterministic\HdPrefix\NetworkConfig;
use BitWasp\Bitcoin\Network\Slip132\BitcoinRegistry;
use BitWasp\Bitcoin\Key\Deterministic\Slip132\Slip132;
use BitWasp\Bitcoin\Key\KeyToScript\KeyToScriptHelper;
use BitWasp\Bitcoin\Serializer\Key\HierarchicalKey\Base58ExtendedKeySerializer;
use BitWasp\Bitcoin\Serializer\Key\HierarchicalKey\ExtendedKeySerializer;

require_once __DIR__ . '/vendor/autoload.php';

$adapter = Bitcoin::getEcAdapter();
$btc = NetworkFactory::bitcoin();
$slip132 = new Slip132(new KeyToScriptHelper($adapter));
$bitcoinPrefixes = new BitcoinRegistry();
$config = new GlobalPrefixConfig([
    new NetworkConfig($btc, [$slip132->p2shP2wpkh($bitcoinPrefixes)]) // p2shP2wpkh (p2wpkh in p2sh) -> ypup
]);
$serializer = new Base58ExtendedKeySerializer(
    new ExtendedKeySerializer($adapter, $config)
);

// master public key from m/49'/0'/10'
$account0masterKey = $serializer->parse($btc, "ypub...");

// address 2 public key from "m/49'/0'/10'/0/1" <- 1=userID
$addrKey = $account0masterKey->derivePath("0/1");
```